# MYSENSORS TFT MODULE

The purpose of this page is to explain step by step the realization of an TFT display based on ARDUINO NANO, with temperature and humidity sensor, connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

The board uses the following components :

 * an ARDUINO NANO
 * an NRF24L01
 * an ILI9341 TFT display
 * an SHT31D module
 * some passive components
 * the board is powered by the USB.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/05/un-afficheur-pour-domoticz-ou-jeedom.html

