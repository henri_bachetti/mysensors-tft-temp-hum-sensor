
#include <Time.h>

//#define MY_DEBUG

#define MY_RADIO_NRF24
//#define MY_RADIO_RFM69

#define MY_RF24_CE_PIN    7
#define MY_RF24_CS_PIN    8
#define BUZ_PIN           2

// uncomment to use motion detector
//#define SCREEN_SAVER
#ifdef SCREEN_SAVER
#define MOTION_SENSOR     A1
#define TFT_POWER_PIN     A2
#endif

#define TEMP_CHILD        0
#define HUM_CHILD         1
#define LCD_CHILD2        9
#define LCD_CHILD1        8
#define LCD_CHILD2        9
#define LCD_CHILD3        10
#define LCD_CHILD4        11
#define BUZ_CHILD         12
#define LCD_CHILD5        13

#include <SPI.h>
#include <MySensors.h>
#include <Adafruit_SHT31.h>
#include "tft.h"

#define COMPARE_TEMP 1 // Send temperature only if changed? 1 = Yes 0 = No
#define TFT_BGD         ILI9341_BLACK

Adafruit_SHT31 sht31 = Adafruit_SHT31();

Text infoText1(NULL, ILI9341_ORANGE, 2, "...");
Text infoText2(NULL, ILI9341_ORANGE, 2, "...");
Text infoText3(NULL, ILI9341_ORANGE, 2, "...");
Text infoText4(NULL, ILI9341_ORANGE, 2, "...");
StatusText infoText5(NULL, ILI9341_LIGHTGREY, ILI9341_BLUE, 2, "...");
Text dateText(NULL, ILI9341_CYAN, 2, "");
Text timeText(NULL, ILI9341_YELLOW, 4, "");
Text tempText(NULL, ILI9341_ORANGE, 3, "");
Text humText(NULL, ILI9341_ORANGE, 2, "");

unsigned long SLEEP_TIME = 30000; // Sleep time between reads (in milliseconds)
boolean timeReceived = false;
// Initialize messages for display
MyMessage textMsg(0, V_TEXT);
MyMessage buzMsg(BUZ_CHILD, V_LIGHT);
int buzzerActive;

unsigned long testFillScreen()
{
  displayTime();
  displayTemp();
  displayInfo();
}

void beep(int n)
{
  for (int i = 0 ; i < n ; i++) {
    digitalWrite(BUZ_PIN, HIGH);
    delay(200);
    digitalWrite(BUZ_PIN, LOW);
    delay(200);
  }
}

void before()
{
  Serial.print(F("MYSENSORS Display & Temperature / Humidity sensor: "));
  pinMode(BUZ_PIN, OUTPUT);
#ifdef SCREEN_SAVER
  pinMode(TFT_POWER_PIN, OUTPUT);
  pinMode(MOTION_SENSOR, INPUT);
  digitalWrite(TFT_POWER_PIN, LOW);
#endif
  beep(1);
  if (!sht31.begin(0x44)) {
    Serial.println(F("Couldn't find SHT31D"));
  }
  tft.begin();
  tft.fillScreen(TFT_BGD);
  yield();
//  tft.setRotation(2);
  Text::setMargin(10);
  testFillScreen();
  Serial.println(F("OK"));
}

void setup()
{
  Serial.begin(115200);
  displayTemp();
  requestTime();
  Serial.println(F("Setup OK"));
}

void presentation() {
  Serial.print(F("presentation: "));
  // Send the sketch version information to the gateway and Controller
  if (sendSketchInfo("TFT display & SHT31D Sensor", "1.1") != true) {
    displayError("Gateway");
    Serial.println("Cannot contact the gateway");
    return;
  }
  present(TEMP_CHILD, S_TEMP);
  present(HUM_CHILD, S_HUM);
  present(LCD_CHILD1, S_INFO, "LCD display 1");    // new S_type 20150905 (not know by domoticz)
  present(LCD_CHILD2, S_INFO, "LCD display 2");
  present(LCD_CHILD3, S_INFO, "LCD display 3");
  present(LCD_CHILD4, S_INFO, "LCD display 4");
  present(LCD_CHILD5, S_INFO, "LCD display 5");
  present(BUZ_CHILD, S_BINARY, "Buzzer");
  send(textMsg.setSensor(LCD_CHILD1).set("-"));  // initialize the V_TEXT at controller for sensor to none (trick for Domoticz)
  send(textMsg.setSensor(LCD_CHILD2).set("-"));
  send(textMsg.setSensor(LCD_CHILD3).set("-"));
  send(textMsg.setSensor(LCD_CHILD4).set("-"));
  send(textMsg.setSensor(LCD_CHILD5).set("-"));
  send(buzMsg.set(0));
  Serial.println("OK");
}

#define RECEIVED_LINE1      0x01
#define RECEIVED_LINE2      0x02
#define RECEIVED_LINE3      0x04
#define RECEIVED_LINE4      0x08
#define RECEIVED_LINE5      0x10
#define RECEIVED_CONFIRMED  0x20
#define RECEIVED_ALL        0x1f

static int infoReceived;
static unsigned long infoPeriod = 5000;

void loop()
{
  static unsigned long lastDisplayOn, lastTimeUpdate = 0, lastRequest = 0, lastTempUpdate = 0;
  static bool motion;
  unsigned long t = millis();
  static bool restored = false;

#ifdef SCREEN_SAVER
  if (infoReceived >= RECEIVED_ALL) {
    if (lastDisplayOn == 0) {
      lastDisplayOn = millis();
    }
    if (digitalRead(MOTION_SENSOR) == HIGH) {
      if ( motion == false) {
        Serial.println(F("motion detected"));
        lastDisplayOn = millis();
      }
      if (digitalRead(TFT_POWER_PIN) == HIGH) {
        digitalWrite(TFT_POWER_PIN, LOW);
      }
      motion = true;
    }
    else {
      if (motion == true) {
        Serial.println(F("motion stopped"));
      }
      motion = false;
      if (lastDisplayOn != 0 && t - lastDisplayOn > 20000) {
        if (digitalRead(TFT_POWER_PIN) == LOW) {
          Serial.println(F("power off TFT"));
          digitalWrite(TFT_POWER_PIN, HIGH);
        }
      }
    }
  }
#endif
  if (!restored) {
    if (loadState(BUZ_CHILD) == 1) {
      buzzerActive = 1;
    }
    else {
      buzzerActive = 0;
    }
    restored  = true;
  }
  if (buzzerActive == 1) {
    beep(4);
    buzzerActive = 2;
  }
  else if (buzzerActive == 2) {
    buzzerActive = 1;
  }
  if ((!timeReceived && t - lastRequest > infoPeriod) || (timeReceived && t - lastRequest > infoPeriod)) {
    // Request time from controller.
    Serial.println(F("requesting time & message"));
    requestTime();
    request(LCD_CHILD1, V_TEXT, 0);          // request new values from controller
    wait(2000, C_SET, V_TEXT);
    request(LCD_CHILD2, V_TEXT, 0);
    wait(2000, C_SET, V_TEXT);
    request(LCD_CHILD3, V_TEXT, 0);
    wait(2000, C_SET, V_TEXT);
    request(LCD_CHILD4, V_TEXT, 0);
    wait(2000, C_SET, V_TEXT);
    request(LCD_CHILD5, V_TEXT, 0);
    wait(2000, C_SET, V_TEXT);
    request(BUZ_CHILD, V_STATUS, 0);
    wait(2000, C_SET, V_STATUS);
    lastRequest = t;
  }

  // Update time and buzzer every second
  if (timeReceived && t - lastTimeUpdate > 1000) {
    displayTime();
    lastTimeUpdate = t;
  }
  if (lastTempUpdate == 0 || t - lastTempUpdate > infoPeriod) {
    displayTemp();
    displayInfo();
    lastTempUpdate = t;
  }
  if (infoReceived == RECEIVED_ALL) {
    Serial.println("All informations received OK");
    infoPeriod = 300000;    // 5 minutes
    infoReceived |= RECEIVED_CONFIRMED;
  }
}

void displayTemp(void)
{
  MyMessage msgTemp(0, V_TEMP);
  MyMessage msgHum(0, V_HUM);

  Serial.println(F("Display temperature && humidity"));
  static float lastTemperature;
  float temperature = sht31.readTemperature();
  float humidity = sht31.readHumidity();

  // Only send data if temperature has changed and no error
#if COMPARE_TEMP == 1
  if (lastTemperature != temperature) {
#else
  {
#endif
    int16_t y = 140;
    Serial.print(temperature);
    Serial.println(humidity);
    dtostrf(temperature, 5, 2, tempText.buf);
    strcat(tempText.buf, "\xf7");
    tempText.printLeft(y);
    dtostrf(humidity, 5, 2, humText.buf);
    strcat(humText.buf, "%");
    y += tempText.getHeight() - humText.getHeight() - 1;
    humText.printRight(y);

    // Send in the new temperature & humidity
    send(msgTemp.setSensor(TEMP_CHILD).set(temperature, 1));
    send(msgHum.setSensor(HUM_CHILD).set(humidity, 1));
    // Save new temperatures for next compare
    lastTemperature = temperature;
    Serial.print(F("transmitted OK: "));
    Serial.print(temperature);
    Serial.println(F(" Celsius"));
  }
}

// This is called when a new time value was received
void receiveTime(unsigned long controllerTime)
{
  unsigned long t = now();
  Serial.print("Time value received: ");
  Serial.println(controllerTime);
  setTime(controllerTime);              // time from controller
  timeReceived = true;
}

void convertUnicode(char *s)
{
  int len = strlen(s);
  for (int i = 0 ; i < len ; i++) {
    if (s[i] == '\xc2' && s[i + 1] == '\xb0') {
      s[i] = '\xf7';
      memcpy(s + i + 1, s + i + 2, len - i - 1);
    }
    if (s[i] == '\xc3' && s[i + 1] == '\xa9') {
      s[i] = 'e';
      memcpy(s + i + 1, s + i + 2, len - i - 1);
    }
  }
}

// This is called when a message is received
void receive(const MyMessage &message)
{
  Serial.println(F("Incomming Message"));
  if (message.type == V_TEXT) {             // Text messages only
    Serial.print(F("Message: ")); Serial.print(message.sensor); Serial.print(", Message: "); Serial.println(message.getString());
    // Write some debug info
    if (message.sensor == LCD_CHILD1) {
      strncpy(infoText1.buf, message.getString(), sizeof(infoText1.buf));
      convertUnicode(infoText1.buf);
      infoReceived |= RECEIVED_LINE1;
    }
    if (message.sensor == LCD_CHILD2) {
      strncpy(infoText2.buf, message.getString(), sizeof(infoText2.buf));
      convertUnicode(infoText2.buf);
      infoReceived |= RECEIVED_LINE2;
    }
    if (message.sensor == LCD_CHILD3) {
      strncpy(infoText3.buf, message.getString(), sizeof(infoText3.buf));
      convertUnicode(infoText3.buf);
      infoReceived |= RECEIVED_LINE3;
    }
    if (message.sensor == LCD_CHILD4) {
      strncpy(infoText4.buf, message.getString(), sizeof(infoText4.buf));
      convertUnicode(infoText4.buf);
      infoReceived |= RECEIVED_LINE4;
    }
    if (message.sensor == LCD_CHILD5) {
      const char *msg = message.getString();
      if (!strncmp(msg, "Error: ", 7)) {
        strncpy(infoText5.buf, msg + 7, sizeof(infoText5.buf));
        infoText5.setBackColor(ILI9341_RED);
        infoText5.setColor(ILI9341_YELLOW);
      }
      else {
        strncpy(infoText5.buf, msg, sizeof(infoText5.buf));
        infoText5.setBackColor(ILI9341_LIGHTGREY);
        infoText5.setColor(ILI9341_BLUE);
      }
      convertUnicode(infoText5.buf);
      infoReceived |= RECEIVED_LINE5;
    }
  }
  if (message.type == V_STATUS) {
    Serial.print(F("Message: ")); Serial.print(message.sensor); Serial.print(", Message: "); Serial.println(message.getBool());
    if (message.getBool() == 1) {
      buzzerActive = true;
    }
    else {
      buzzerActive = false;
    }
    saveState(message.sensor, message.getBool());
  }
}

void displayTime(void)
{
  int16_t x = 0, y = 10, x1, y1, w, h;
  char buf[21]; // temp buffer for max 20 char display

  Serial.println(F("Display date & time"));
  snprintf(dateText.buf, sizeof(dateText.buf), "%s %02d-%02d-%04d", dayShortStr(weekday()), day(), month(), year());
  y += dateText.printCentered(y);
  y += 40;
  snprintf(timeText.buf, sizeof(timeText.buf), "%02d:%02d", hour(), minute());
  timeText.printCentered(y);
}

void displayInfo(void)
{
  int16_t y = 188;

  Serial.println(F("Display informations"));
  y += infoText1.printLeft(y);
  y += 10;
  y += infoText2.printLeft(y);
  y += 10;
  y += infoText3.printLeft(y);
  y += 10;
  y += infoText4.printLeft(y);
  y += 15;
  y += infoText5.printCentered(0);
}

void displayError(const char *msg)
{
  strncpy(infoText5.buf, "Error: ", sizeof(infoText5.buf));
  strncpy(infoText5.buf + 7, msg, sizeof(infoText5.buf) - 7);
  infoText5.printCentered(0);
}
