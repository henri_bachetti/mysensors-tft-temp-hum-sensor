

#include "tft.h"

//#define MY_DEBUG

#define TFT_DC 5
#define TFT_CS 10
#define TFT_MOSI 4
#define TFT_CLK A0
#define TFT_RST 6
#define TFT_MISO 12

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);

uint16_t Text::_margin = 10;

Text::Text(GFXfont *font, uint16_t color, uint16_t size, const char *txt)
{
  _font = font;
  _color = color;
  _size = size;
  _prevx = 0;
  _prevy = 0;
  if (!strlen(txt)) {
    txt = " ";
  }
  else {
    strncpy(buf, txt, MAX_TEXT);
  }
  strcpy(_prevbuf, " ");
}

void Text::setMargin(uint16_t margin)
{
  Text::_margin = margin;
}

uint16_t Text::print(uint16_t x, uint16_t y)
{
  int16_t x1, y1;
  uint16_t w, h;

  if (_prevx == 0) {
    _prevx = x;
  }
  if (_prevy == 0) {
    _prevy = y;
  }
#ifdef MY_DEBUG
  Serial.print(F("set font "));
  Serial.println((unsigned int)_font, HEX);
#endif
  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(_prevbuf, 0, 0, &x1, &y1, &w, &h);
  if (strcmp(buf, _prevbuf)) {
    tft.fillRect(_prevx, _prevy, w, h, ILI9341_BLACK);
    tft.setCursor(x, y);
    tft.setTextColor(_color);
    tft.print(buf);
    _prevx = x;
    _prevy = y;
    strcpy(_prevbuf, buf);
  }
//  Serial.print(F("Text::print return "));
  return h;
}

uint16_t Text::printCentered(uint16_t y)
{
  int16_t x = 0;
  int16_t x1, y1;
  uint16_t w, h;

#ifdef MY_DEBUG
  Serial.print(F("Text::printCentered \""));
  Serial.print(buf);
  Serial.print(F("\" AT "));
  Serial.print(y);
#endif
  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
#ifdef MY_DEBUG
  Serial.print(F(" width="));
  Serial.println(w);
#endif
  x = (TFT_WIDTH - w) / 2;
  return print(x, y);
}

uint16_t Text::printLeft(uint16_t y)
{
  int16_t x = 0;
  int16_t x1, y1;
  uint16_t w, h;

#ifdef MY_DEBUG
  Serial.print(F("Text::printLeft \""));
  Serial.print(buf);
  Serial.print(F("\" AT "));
  Serial.print(y);
#endif
  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
#ifdef MY_DEBUG
  Serial.print(F(" width="));
  Serial.println(w);
#endif
  x = _margin;
  return print(x, y);
}

uint16_t Text::printRight(uint16_t y)
{
  int16_t x = 0;
  int16_t x1, y1;
  uint16_t w, h;

#ifdef MY_DEBUG
  Serial.print(F("Text::printRight \""));
  Serial.print(buf);
  Serial.print("\" AT ");
  Serial.print(y);
#endif
  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
#ifdef MY_DEBUG
  Serial.print(" width=");
  Serial.println(w);
#endif
  x = TFT_WIDTH - w - _margin;
  return print(x, y);
}

uint16_t Text::getHeight(void)
{
  int16_t x = 0;
  int16_t x1, y1;
  uint16_t w, h;

  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
  return h;
}

uint16_t Text::getWidth(void)
{
  int16_t x1, y1;
  uint16_t w, h;

  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
  return w;
}

StatusText::StatusText(GFXfont *font, uint16_t backColor, uint16_t color, uint16_t size, const char *txt)
  : Text::Text(font, color, size, txt)
{
  _backColor = backColor;
}

uint16_t StatusText::print(uint16_t x, uint16_t y)
{
  int16_t x1, y1;
  uint16_t w, h;

#ifdef MY_DEBUG
  Serial.println(F("StatusText::print"));
#endif
  tft.setFont(_font);
  tft.setTextSize(_size);
  if (strcmp(buf, _prevbuf)) {
    tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
    tft.fillRect(0, TFT_HEIGHT - h - 4, TFT_WIDTH, h + 4, _backColor);
    tft.setCursor(x, TFT_HEIGHT - h - 2);
    tft.setTextColor(_color);
    tft.print(buf);
    strcpy(_prevbuf, buf);
  }
  return h;
}
