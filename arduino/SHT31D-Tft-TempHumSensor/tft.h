#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"

#define TFT_WIDTH 240
#define TFT_HEIGHT 320
#define MAX_TEXT  21

class Text
{
protected:
  GFXfont *_font;
  uint16_t _color;
  uint16_t _backColor;
  uint16_t _prevx;
  uint16_t _prevy;
  uint16_t _size;
  char _prevbuf[MAX_TEXT];
  static uint16_t _margin;

public:
  Text(GFXfont *font, uint16_t color, uint16_t size, const char *txt);
  static void setMargin(uint16_t margin);
  virtual uint16_t print(uint16_t x, uint16_t y);
  uint16_t printLeft(uint16_t y);
  uint16_t printRight(uint16_t y);
  uint16_t printCentered(uint16_t y);
  void setColor(uint16_t color) {_color = color;}
  uint16_t getHeight(void);
  uint16_t getWidth(void);
  char buf[MAX_TEXT];
};

class StatusText : public Text
{
private:
  uint16_t _backColor;

public:
  StatusText(GFXfont *font, uint16_t backColor, uint16_t color, uint16_t size, const char *txt);
  uint16_t print(uint16_t x, uint16_t y);
  void setBackColor(uint16_t color) {_backColor = color;}
};

extern Adafruit_ILI9341 tft;

