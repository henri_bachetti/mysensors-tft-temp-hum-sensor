#!/usr/bin/python
# - *- coding: utf- 8 - *-
import urllib
from domoticz import Domoticz

devices = {u'Thermomètre mobile': 'Up Therm Batt', u'Temp & Humidité Extérieure': 'Out Therm Batt', u'Détecteur escalier': 'Motion Batt'}

url = 'http://192.168.1.134:8080'
domo = Domoticz(url)
buzzer = domo.getDeviceId(u'command', u'Buzzer')
print 'buzzer: ', buzzer
display = domo.getDeviceId(u'utility', u'LCD display 5')
print 'display: ', display
activate = 'Off'
message = 'Everything%20is%20Fine'
temp = domo.getDeviceInfo(u'temp', u'Congélateur', 'Temp')
print 'Freezer temp:', temp
if temp > -18:
        print 'Freezer temp too high'
        activate = 'On'
        message = 'Error:%20Freezer%20Temp%20:%20High'
else:
        print 'Freezer temp OK'

for device in devices.keys():
        battLevel = domo.getDeviceInfo(u'all', device, 'BatteryLevel')
        print 'Battery level:', battLevel
        if battLevel < 20:
                print 'Battery too low'
                message = 'Error:%20' + urllib.quote(devices[device])
                activate = 'On'
        else:
                print 'Battery OK'

domo.setDeviceCommand(buzzer, activate)
print message
domo.setDeviceInfo(display, message)

