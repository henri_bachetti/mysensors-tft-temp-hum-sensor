#!/usr/bin/python
# - *- coding: utf- 8 - *-
import urllib
from domoticz import Domoticz
 
url = "http://192.168.1.134:8080"
domo = Domoticz(url)
temp = domo.getDeviceInfo(u'temp', u'Congélateur', 'Temp')
print 'Temp:', temp
display = domo.getDeviceId(u'utility', u'LCD display 4')
print 'display: ', display
data = u'Freezer%%3a%%20%2.1f%s' % (temp, urllib.quote(u"°".encode('utf8')))
print data
domo.setDeviceInfo(display, data)
