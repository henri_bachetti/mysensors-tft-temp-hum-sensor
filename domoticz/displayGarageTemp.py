#!/usr/bin/python
# - *- coding: utf- 8 - *-
import urllib
from domoticz import Domoticz
 
url = "http://192.168.1.134:8080"
domo = Domoticz(url)
garageTemp = domo.getDeviceInfo(u'temp', u'Thermomètre mobile', 'Temp')
print 'outHum:', garageTemp
display = domo.getDeviceId(u'utility', u'LCD display 2')
print 'display: ', display
data = u'Garage%%3a%%20%2d%s' % (garageTemp, urllib.quote("°"))
print data
domo.setDeviceInfo(display, data)
