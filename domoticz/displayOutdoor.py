#!/usr/bin/python
# - *- coding: utf- 8 - *-
import urllib
from domoticz import Domoticz

url = "http://192.168.1.134:8080"
domo = Domoticz(url)
outTemp = float(domo.getDeviceInfo(u'temp', u'Temp & Humidité Extérieure', 'Temp'))
outHum = domo.getDeviceInfo(u'temp', u'Temp & Humidité Extérieure', 'Humidity')
print 'out:', outTemp, outHum
display = domo.getDeviceId(u'utility', u'LCD display 3')
print 'display: ', display
data = u'Out%%3a%%20%2.1f%s%%20%d%s' % (outTemp, urllib.quote(u"°".encode('utf8')), outHum, urllib.quote("%"))
print data
domo.setDeviceInfo(display, data)
