#!/usr/bin/python
# - *- coding: utf- 8 - *-
import urllib
from domoticz import Domoticz

url = "http://192.168.1.134:8080"
domo = Domoticz(url)
upTemp = float(domo.getDeviceInfo(u'temp', u'Température Etage', 'Temp'))
print 'upTemp:', upTemp
display = domo.getDeviceId(u'utility', u'LCD display 1')
print 'display: ', display
data = u'Up%%3a%%20%2.1f%s' % (upTemp, urllib.quote(u"°".encode('utf8')))
print data
domo.setDeviceInfo(display, data)

