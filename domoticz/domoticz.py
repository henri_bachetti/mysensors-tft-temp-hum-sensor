#!/usr/bin/python
# - *- coding: utf- 8 - *-
import sys
import json
import urllib2
import re
import time
import datetime
import httplib, urllib

class Domoticz():
 
	def __init__(self, url):
		self.baseurl = url

	def __execute__(self, url):
		req = urllib2.Request(url)
		return urllib2.urlopen(req, timeout=5)

	def getDeviceId(self, filter, name):
		"""
		Get the Domoticz device id
		"""
		url = u"%s/json.htm?type=devices&filter=%s&used=true&order=Name&" % (self.baseurl, filter)
		print url
		data = json.load(self.__execute__(url))
		for device in data['result']:
			if device['Name'] == name:
				return device['idx']
		return None

	def getDeviceInfo(self, filter, name, info):
		"""
		Get the Domoticz device information.
		"""
		url = u"%s/json.htm?type=devices&filter=%s&used=true&order=Name&" % (self.baseurl, filter)
		print url
		data = json.load(self.__execute__(url))
		for device in data['result']:
			if device['Name'] == name:
				return device[info]
		return None

	def setDeviceCommand(self, xid, command):
		"""
		Set the Domoticz device information.
		"""
		url = "%s/json.htm?type=command&param=switchlight&idx=%s&switchcmd=%s&" % (self.baseurl, xid, command)
		print url
		data = json.load(self.__execute__(url))
		return data
 
	def setDeviceInfo(self, xid, info):
		"""
		Set the Domoticz device information.
		"""
		url = "%s/json.htm?type=command&param=udevice&idx=%s&nvalue=0&svalue=%s&" % (self.baseurl, xid, info)
		print url
		data = json.load(self.__execute__(url))
		return data
 
